<?php 

namespace App;

use CORE\Init\Bootstrap;

class Init extends Bootstrap
{
	protected function initRoutes()
	{
		$route['home'] = ['route' => '/', 'controller' => 'index', 'action' => 'index'];
		
		$route['login']  = ['route' => '/login', 'controller' => 'login', 'action' => 'index'];
		$route['auth']   = ['route' => '/auth', 'controller' => 'login', 'action' => 'auth'];
		$route['logout'] = ['route' => '/logout', 'controller' => 'login', 'action' => 'logout'];

		$route['preview'] = ['route' => '/preview', 'controller' => 'preview', 'action' => 'index'];
		$route['profile'] = ['route' => '/profile', 'controller' => 'profile', 'action' => 'index'];

        $route['company']        = ['route' => '/company', 'controller' => 'company', 'action' => 'index'];
        $route['company-update'] = ['route' => '/company/update', 'controller' => 'company', 'action' => 'update'];

        $route['about']        = ['route' => '/about', 'controller' => 'about', 'action' => 'index'];
		$route['about-update'] = ['route' => '/about/update', 'controller' => 'about', 'action' => 'update'];

        $route['banner']         = ['route' => '/banner', 'controller' => 'banner', 'action' => 'index'];
        $route['banner-new']     = ['route' => '/banner/new', 'controller' => 'banner', 'action' => 'new'];
        $route['banner-edit']    = ['route' => '/banner/edit', 'controller' => 'banner', 'action' => 'edit'];
		$route['banner-delete']  = ['route' => '/banner/delete', 'controller' => 'banner', 'action' => 'delete'];
        $route['banner-grid']    = ['route' => '/banner/grid', 'controller' => 'banner', 'action' => 'grid'];
        $route['banner-preview'] = ['route' => '/banner/preview', 'controller' => 'banner', 'action' => 'preview'];
        $route['banner-export']  = ['route' => '/banner/export', 'controller' => 'banner', 'action' => 'export'];
        $route['banner-search']  = ['route' => '/banner/search', 'controller' => 'banner', 'action' => 'search'];

        $route['service']         = ['route' => '/service', 'controller' => 'service', 'action' => 'index'];
        $route['service-new']     = ['route' => '/service/new', 'controller' => 'service', 'action' => 'new'];
        $route['service-edit']    = ['route' => '/service/edit', 'controller' => 'service', 'action' => 'edit'];
        $route['service-delete']  = ['route' => '/service/delete', 'controller' => 'service', 'action' => 'delete'];
        $route['service-grid']    = ['route' => '/service/grid', 'controller' => 'service', 'action' => 'grid'];
        $route['service-preview'] = ['route' => '/service/preview', 'controller' => 'service', 'action' => 'preview'];
        $route['service-export']  = ['route' => '/service/export', 'controller' => 'service', 'action' => 'export'];
        $route['service-search']  = ['route' => '/service/search', 'controller' => 'service', 'action' => 'search'];

        $route['gallery']         = ['route' => '/gallery', 'controller' => 'gallery', 'action' => 'index'];
        $route['gallery-new']     = ['route' => '/gallery/new', 'controller' => 'gallery', 'action' => 'new'];
        $route['gallery-edit']    = ['route' => '/gallery/edit', 'controller' => 'gallery', 'action' => 'edit'];
        $route['gallery-delete']  = ['route' => '/gallery/delete', 'controller' => 'gallery', 'action' => 'delete'];
        $route['gallery-grid']    = ['route' => '/gallery/grid', 'controller' => 'gallery', 'action' => 'grid'];
        $route['gallery-preview'] = ['route' => '/gallery/preview', 'controller' => 'gallery', 'action' => 'preview'];
        $route['gallery-export']  = ['route' => '/gallery/export', 'controller' => 'gallery', 'action' => 'export'];
        $route['gallery-search']  = ['route' => '/gallery/search', 'controller' => 'gallery', 'action' => 'search'];

        $route['user']         = ['route' => '/user', 'controller' => 'user', 'action' => 'index'];
        $route['user-grid']    = ['route' => '/user/grid', 'controller' => 'user', 'action' => 'grid'];
        $route['user-new']     = ['route' => '/user/new', 'controller' => 'user', 'action' => 'new'];
        $route['user-edit']    = ['route' => '/user/edit', 'controller' => 'user', 'action' => 'edit'];
        $route['user-save']    = ['route' => '/user/save', 'controller' => 'user', 'action' => 'save'];
        $route['user-update']  = ['route' => '/user/update', 'controller' => 'user', 'action' => 'update'];
        $route['user-delete']  = ['route' => '/user/delete', 'controller' => 'user', 'action' => 'delete'];
        $route['user-preview'] = ['route' => '/user/preview', 'controller' => 'user', 'action' => 'preview'];
        $route['user-export']  = ['route' => '/user/export', 'controller' => 'user', 'action' => 'export'];
        $route['user-search']  = ['route' => '/user/search', 'controller' => 'user', 'action' => 'search'];
        $route['user-change-password'] = ['route' => '/user/change-password', 'controller' => 'user', 'action' => 'change-password'];
        $route['user-update-password'] = ['route' => '/user/update-password', 'controller' => 'user', 'action' => 'update-password'];

		$this->setProjectUrl("/webadmin");
		$this->setRoutes($route);
	}

	public static function getDb()
	{
		$db = new \PDO(
			"mysql:host=localhost;dbname=sis", "root", "root",
			[\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]
		);
		$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		return $db;
	}
}