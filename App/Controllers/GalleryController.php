<?php 

namespace App\Controllers;

use CORE\Controller\ActionController;
use CORE\Pagination\Paginator;
use PHPExcel;
use WideImage;

class GalleryController extends ActionController
{
    public function indexAction()
    {
        return $this->render('index');
    }

	public function gridAction()
	{
        $pagination = new Paginator($this->conn);
        $pagination->setSQL("SELECT * FROM gallery");
        $pagination->setPaginator('page');

        $result = $this->conn->query($pagination->getSQL());

        $this->view->paginator = $pagination;
        $this->view->galleries = $result;

        return $this->render('grid', false);
	}

    public function newAction()
    {
        if (!empty($_POST)) {
            if (!empty($_FILES['file']['name'])) {
                $image = $_FILES['file']['name'];
                $dir   = "../public/uploads/gallery/";
                if (move_uploaded_file($_FILES['file']['tmp_name'], $dir . $image)) {
                    $wiAction = WideImage::load($dir . $image);
                    $wiAction = $wiAction->resize(800, 600, true);
                    $wiAction->saveToFile("../public/uploads/gallery/" . $image);
                }
            } else {
                $image = null;
            }

            $currentDate = date("Y-m-d H:i:s");

            $fields = [
                'title', 'caption', 'image', 'status', 'created_at', 'updated_at'
            ];

            $values = [
                $_POST['title'], $_POST['caption'], $image, $_POST['status'], $currentDate, $currentDate
            ];

            $crud   = \CORE\Di\Container::getClass("Gallery");
            $insert = $crud->insert($fields, $values);

            if ($insert) {
                return $this->redirect("gallery");
            }
        }

        return $this->render('new');
    }

    public function editAction()
    {
        $id     = (int)$_GET['id'];
        $crud   = \CORE\Di\Container::getClass("Gallery");
        $gallery = $crud->find($id);

        if (!empty($_POST)) {
            if (!empty($_FILES['file']['name'])) {
                $image = $_FILES['file']['name'];
                $dir   = "../public/uploads/gallery/";
                if (move_uploaded_file($_FILES['file']['tmp_name'], $dir . $image)) {
                    $wiAction = WideImage::load($dir . $image);
                    $wiAction = $wiAction->resize(800, 400, true);
                    $wiAction->saveToFile("../public/uploads/gallery/" . $image);
                }
            } else {
                $image = $gallery['image'];
            }

            $data = [
                'title' => $_POST['title'],
                'caption' => $_POST['caption'],
                'image' => $image,
                'status' => $_POST['status'],
                'updated_at' => date("Y-m-d H:i:s")
            ];

            if ($crud->update($data, $id))
                return $this->redirect("gallery");

        }
        
        $this->view->data = $gallery;
        return $this->render('edit');
    }

    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $crud = \CORE\Di\Container::getClass("Gallery");
        return $crud->delete($id);
    }

    public function searchAction()
    {
        $conditonal = " WHERE title LIKE '%" . $_GET['title'] . "%' 
							AND caption LIKE '%" . $_GET['caption'] . "%'";

        if ($_GET['dtStart'] != "" && $_GET['dtFinal'] != "") {
            $expStartDate = explode('/', $_GET['dtStart'], 3);
            $startDate = $expStartDate[2] . '-' . $expStartDate[1] . '-' . $expStartDate[0] . ' 00:00:00';

            $expFinalDate = explode('/', $_GET['dtFinal'], 3);
            $finalDate = $expFinalDate[2] . '-' . $expFinalDate[1] . '-' . $expFinalDate[0] . ' 23:59:59';

            $conditonal .= " AND created_at BETWEEN '" . $startDate . "' AND '" . $finalDate . "' ";
        }

        $query  = ("SELECT id, title, caption FROM gallery {$conditonal} ORDER BY id ");
        $result = $this->conn->query($query);

        $this->view->result = $result;
        return $this->render('search', false);
    }

    public function previewAction()
    {
        $id    = (int)$_GET['id'];
        $crud  = \CORE\Di\Container::getClass("Gallery");
        $gallery  = $crud->find($id);

        $this->view->prewiew = $gallery;
        return $this->render('preview', false);
    }

    public function exportAction()
    {
        $gallerys  = self::getData();

        $phpExcel = new PHPExcel();

        $phpExcel->getProperties()->setCreator("Sis")
            ->setTitle("Relatório de Gallery")
            ->setDescription("Lista de fotos cadastradas no sistema.");

        $phpExcel->getActiveSheet()->setTitle('Relatório de Galeria');

        $phpExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'TÍTULO')
            ->setCellValue('B1', 'LEGENDA')
            ->setCellValue('C1', 'IMAGEM')
            ->setCellValue('D1', 'STATUS')
            ->setCellValue('E1', 'DATA/HORA CADASTRO');

        $counter = 1;
        foreach ($gallerys as $gallery) {
            $counter++;

            $status = ($gallery['status'] == "A") ? "Ativo" : "Inativo";

            $phpExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $counter, $gallery['title'])
                ->setCellValue('B' . $counter, $gallery['caption'])
                ->setCellValue('C' . $counter, $gallery['image'])
                ->setCellValue('D' . $counter, $status)
                ->setCellValue('E' . $counter, $this->formatTimestamp($gallery['created_at']));
        }

        // Rename worksheet
        $phpExcel->getActiveSheet()->setTitle('Galeria');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcel->setActiveSheetIndex(0);

        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');

        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="Relatório-Galeria' . time() . '.xlsx"');

        $file =	$objWriter->save('php://output');

        return $this->render('export', false);
    }

    private function formatTimestamp($timestamp)
    {
        $exp  = explode('-', $timestamp);
        $day  = substr($exp[2], 0, 2);
        $time = substr($exp[2], 2);
        $date = $day . '/' . $exp[1] . '/' . $exp[0] . ' ' . $time;
        return $date;
    }

    private static function getData()
    {
        $crud = \CORE\Di\Container::getClass("Gallery");
        return $crud->findAll();
    }
}