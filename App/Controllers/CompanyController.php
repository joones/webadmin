<?php 

namespace App\Controllers;

use CORE\Controller\ActionController;
use WideImage;

class CompanyController extends ActionController
{
	public function indexAction()
	{
		$this->view->data = self::getData();
		return $this->render('index');
	}

    public function updateAction()
    {
        if (!empty($_FILES['file']['name'])) {
            $image = $_FILES['file']['name'];
            $dir   = "../public/uploads/company/";
            if (move_uploaded_file($_FILES['file']['tmp_name'], $dir . $image)) {
                $wiAction = WideImage::load($dir . $image);
                $wiAction = $wiAction->resize(400, 380, true);
                $wiAction->saveToFile("../public/uploads/company/" . $image);
            }
        } else {
            $data  = self::getData();
            $image = $data['logo'];
        }

        $data = [
            'name' => $_POST['name'],
            'city' => $_POST['city'],
            'logo' => $image,
            'email_1' => $_POST['email_1'],
            'email_2' => $_POST['email_2'],
            'phone_1' => $_POST['phone_1'],
            'phone_2' => $_POST['phone_2'],
            'cellphone' => $_POST['cellphone'],
            'facebook' => $_POST['facebook'],
            'twitter' => $_POST['twitter'],
            'youtube' => $_POST['youtube'],
            'gplus' => $_POST['gplus'],
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $crud = \CORE\Di\Container::getClass("Company");
        $crud->update($data, 1);

        return $this->redirect("company");
    }

    private static function getData()
    {
        $crud = \CORE\Di\Container::getClass("Company");
        return $crud->find(1);
    }
}