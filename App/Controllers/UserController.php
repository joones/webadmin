<?php 

namespace App\Controllers;

use CORE\Controller\ActionController;
use PHPExcel;
use CORE\Pagination\Paginator;

class UserController extends ActionController
{
	public function indexAction()
	{
		return $this->render('index');
	}

	public function gridAction()
	{
		$pagination = new Paginator($this->conn);
		$pagination->setSQL("SELECT id, name, email FROM user ORDER BY id");
		$pagination->setPaginator('page');
		$results = $this->conn->query($pagination->getSQL());
		$this->view->paginator = $pagination;
		$this->view->user = $results;
		return $this->render('grid', false);
	}

	public function searchAction()
	{
		$conditonal = " WHERE name LIKE '%" . $_GET['name'] . "%' 
							AND email LIKE '%" . $_GET['email'] . "%'";

		if ($_GET['dtStart'] != "" && $_GET['dtFinal'] != "") {
			$expStartDate = explode('/', $_GET['dtStart'], 3);
			$startDate = $expStartDate[2] . '-' . $expStartDate[1] . '-' . $expStartDate[0] . ' 00:00:00';

			$expFinalDate = explode('/', $_GET['dtFinal'], 3);
			$finalDate = $expFinalDate[2] . '-' . $expFinalDate[1] . '-' . $expFinalDate[0] . ' 23:59:59';

			$conditonal .= " AND created_at BETWEEN '" . $startDate . "' AND '" . $finalDate . "' ";
		}					

		$query = ("SELECT id, name, email FROM user {$conditonal} ORDER BY id ");
		$results = $this->conn->query($query);
		$this->view->user = $results;
		return $this->render('search', false);
	}

	public function newAction()
	{
		return $this->render('new', false);
	}

	public function saveAction()
	{
		$currentDate = date("Y-m-d H:i:s");

		$fields = [
			'name', 'email', 'password', 'status', 'created_at', 'updated_at'
		];

		$values = [
			$_GET['name'], $_GET['email'], md5($_GET['password']), $_GET['status'], $currentDate, $currentDate
		];

		$crud = \CORE\Di\Container::getClass("User");
		$insert  = $crud->insert($fields, $values);

		return $insert;
	}

	public function editAction()
	{
		$id = (int)$_GET['id'];
		$crud = \CORE\Di\Container::getClass("User");
		$user  = $crud->find($id);
		$this->view->user = $user;
		return $this->render('edit', false);
	}

	public function updateAction()
	{
		$id = (int)$_GET['id'];
		$currentDate = date("Y-m-d H:i:s");

		$data = [
			'name' => $_GET['name'],
			'email' => $_GET['email'],
			'status' => $_GET['status'],
			'updated_at' => $currentDate
		];

		$crud = \CORE\Di\Container::getClass("User");
		$update  = $crud->update($data, $id);

		return $update;
	}

	public function deleteAction()
	{
		$id = (int)$_GET['id'];
		$crud = \CORE\Di\Container::getClass("User");
		$delete  = $crud->delete($id);
		return true;
	}

	public function changePasswordAction()
	{
		$id    = (int)$_GET['id'];
		$crud  = \CORE\Di\Container::getClass("User");
		$user  = $crud->find($id);

		$this->view->user = $user;
		return $this->render('change-password', false);
	}

	public function updatePasswordAction()
	{
		$id    = (int)$_GET['id'];

		$data = [
			'password' => $_GET['pass'], 
			'updated_at' => '2014-06-11 14:00:00'
		];

		$crud = \CORE\Di\Container::getClass("User");
		$update  = $crud->update($data, $id);

		return $update;
	}

	public function previewAction()
	{
		$id    = (int)$_GET['id'];
		$crud  = \CORE\Di\Container::getClass("User");
		$user  = $crud->find($id);

		$this->view->prewiew = $user;
		return $this->render('preview', false);
	}

	public function exportAction()
	{
		$crud  = \CORE\Di\Container::getClass("User");
		$users  = $crud->findAll();

		$phpExcel = new PHPExcel();

		$phpExcel->getProperties()->setCreator("Sis")
	            ->setTitle("Relatório de Usuários")
	            ->setDescription("Lista de usuários cadastrados no sistema.");

		$phpExcel->getActiveSheet()->setTitle('Relatório de Usuários');

		$phpExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'NOME')
            ->setCellValue('B1', 'EMAIL')
            ->setCellValue('C1', 'STATUS')
            ->setCellValue('D1', 'DATA/HORA CADASTRO');

        $counter = 1;
        foreach ($users as $user) {
        	$counter++;

        	$status = ($user['status'] == "A") ? "Ativo" : "Inativo";

			$phpExcel->setActiveSheetIndex(0)
					->setCellValue('A' . $counter, $user['name'])
					->setCellValue('B' . $counter, $user['email'])
		            ->setCellValue('C' . $counter, $status)
					->setCellValue('D' . $counter, $this->formatTimestamp($user['created_at']));
        }    

		// Rename worksheet
		$phpExcel->getActiveSheet()->setTitle('Usuários');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$phpExcel->setActiveSheetIndex(0);

		$objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');

		header('Content-type: application/vnd.ms-excel');

		// It will be called file.xls
		header('Content-Disposition: attachment; filename="Relatório-Usuários' . time() . '.xlsx"');

		$file =	$objWriter->save('php://output');

		return $this->render('export', false);
	}

	private function formatTimestamp($timestamp)
	{
		$exp  = explode('-', $timestamp); 
		$day  = substr($exp[2], 0, 2);
		$time = substr($exp[2], 2);
		$date = $day . '/' . $exp[1] . '/' . $exp[0] . ' ' . $time;
		return $date;
	}
}