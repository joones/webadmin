<?php 

namespace App\Controllers;

use CORE\Controller\ActionController;

class IndexController extends ActionController
{
	public function indexAction()
	{
		$this->render('index');
	}
}