<?php 

namespace App\Controllers;

use CORE\Controller\ActionController;
use WideImage;

class AboutController extends ActionController
{
	public function indexAction()
	{
		$this->view->data = self::getData();
		return $this->render('index');
	}

    public function updateAction()
    {
        if (!empty($_FILES['file']['name'])) {
            $image = $_FILES['file']['name'];
            $dir   = "../public/uploads/about/";
            if (move_uploaded_file($_FILES['file']['tmp_name'], $dir . $image)) {
                $wiAction = WideImage::load($dir . $image);
                $wiAction = $wiAction->resize(400, 380, true);
                $wiAction->saveToFile("../public/uploads/about/" . $image);
            }
        } else {
            $data  = self::getData();
            $image = $data['image'];
        }

        $data = [
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'image' => $image,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $crud = \CORE\Di\Container::getClass("About");
        $crud->update($data, 1);

        return $this->redirect("about");
    }

    private static function getData()
    {
        $crud = \CORE\Di\Container::getClass("About");
        return $crud->find(1);
    }
}