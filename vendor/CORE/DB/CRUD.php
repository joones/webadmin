<?php 

namespace CORE\Db;

abstract class CRUD
{
	protected $db;
	protected $table;

	public function __construct(\PDO $db)
	{
		$this->db = $db;
	}

	public function insert($fields, $values)
	{
		try {
			$f = implode(",", $fields);
			$v = implode("','", $values);
			$v = "'" . $v . "'";

			$query = "INSERT INTO {$this->table} ({$f}) VALUES ({$v})";
			$stmt = $this->db->prepare($query);
			return $stmt->execute();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function update($data, $id)
	{
		try {
			$query = "UPDATE {$this->table} SET ";
			$totalItens = count($data);
			$counter = 0;

			foreach ($data as $label => $val) {
				$counter++;
				$comma = ($counter < $totalItens) ? ", " : "";
				$key = $label . "=:" . $label . $comma;
				$query .= $key;
			}

			$query .= " WHERE id=:id";
			$stmt = $this->db->prepare($query);

			foreach ($data as $column => $value) {
				$stmt->bindValue(":" . $column, $value);
			}
			$stmt->bindValue(":id", $id);
			$update = $stmt->execute();

			return $update;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function delete($id)
	{
		try {
			$query = "DELETE FROM {$this->table} WHERE id=:id";
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id", $id);
			return $stmt->execute();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function findAll()
	{
		try {
			$query = "SELECT * FROM {$this->table}";
			return $this->db->query($query);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function find($id) 
	{
		try {
			$query = "SELECT * FROM {$this->table} WHERE id=:id";
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id", $id);
			$stmt->execute();
			return $stmt->fetch();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function findUserByCredentials($email, $pass) 
	{
		try {
			$query = "SELECT name, email, password FROM user WHERE email=:email AND password=:password";
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":email", $email);
			$stmt->bindValue(":password", $pass);
			$stmt->execute();

			if ($stmt->rowCount() == 1) {
				return $stmt->fetch();
			} else {
				return false;		
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
}