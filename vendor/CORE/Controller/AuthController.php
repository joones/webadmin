<?php 

namespace CORE\Controller;

use App\Models\User;

class AuthController
{
	public function __construct()
	{
		$this->setSessionId();
	}

	public function getIdentity()
	{
		$userSession = $this->checkUserSession();
		return $userSession;
	}

	private function checkUserSession()
	{
		if (isset($_SESSION['EMAIL']) && isset($_SESSION['PASS'])) {
			$email = $_SESSION['EMAIL'];
			$pass  = $_SESSION['PASS'];
		} else {
			$email = "";
			$pass  = "";
		}

		$validate = $this->validateCredentials($email, $pass);
		return $validate;
	}

	private function setSessionId()
	{
		if (!session_id()) {
			return session_start();
		}
	}

	private function validateCredentials($email, $pass)
	{
		$user      = \CORE\Di\Container::getClass("User");
		$validate  = $user->findUserByCredentials($email, $pass);
		return $validate ;
	}
}