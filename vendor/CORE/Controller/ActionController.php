<?php 

namespace CORE\Controller;

class ActionController 
{
	protected $view;
	protected $action;
	protected $conn;

	public function __construct()
	{
		$this->view = new \stdClass;
		$this->conn = \App\Init::getDb();
	}

	public function render($action, $layout = true)
	{
		$this->action = $action;
		if ($layout == true && file_exists("../App/views/layout/layout.phtml")) {
			include_once "../App/views/layout/layout.phtml";
		} else {
			$this->content();
		}
	}

	public function content()
	{
		$actualClass 	 = get_class($this);
		$singleClassName = strtolower(str_replace("App\\Controllers\\", "", $actualClass));
		$className       = str_replace("controller", "", $singleClassName);
		include_once '../App/views/' . $className . '/' . $this->action . '.phtml';
	}

	public function redirect($module)
	{
		return header("Location: " . baseUrl . $module);
	}
}