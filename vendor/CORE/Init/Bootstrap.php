<?php 

namespace CORE\Init;

use CORE\Controller\AuthController;

abstract class Bootstrap
{
	private $routes;
	private $projectUrl;

	public function __construct()
	{
		$this->initRoutes();
		$this->run($this->getUrl());
	}

	abstract protected function initRoutes();

	protected function run($url)
	{
		$auth = new AuthController();
		if ($auth->getIdentity()) {
			array_walk($this->routes, function ($route) use ($url) {
                if ($url == $this->projectUrl . $route['route']) {
                    $class = "App\\Controllers\\" . ucfirst($route['controller']) . "Controller";
					$controller = new $class;

					$hasTwoNames = strpos($route['action'], '-');

					if ($hasTwoNames) {
						$exp = explode('-', $route['action'], 2);
						$actionName = $exp[0] . ucfirst($exp[1]);
					} else {
						$actionName = $route['action'];
					}

					$action = $actionName . "Action";
					$controller->$action();
				}
			});
		} else {
			if ($url != "/webadmin/auth") {
				$login = new \App\Controllers\LoginController();
				return $login->indexAction();
			} else {
				$login = new \App\Controllers\LoginController();
				return $login->authAction();
			}
		}
	}

	protected function setRoutes(array $routes)
	{
		$this->routes = $routes;
	}

	protected function setProjectUrl($projectUrl)
	{
		$this->projectUrl = $projectUrl;
	}

	protected function getUrl()
	{
		return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	}
}