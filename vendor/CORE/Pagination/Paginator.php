<?php 

namespace CORE\Pagination;

use PDO_Pagination;

class Paginator extends PDO_Pagination
{

    public function printResultBar() 
    {         
        if ($this->getTotalOfResults() > 0) { 

            echo '<div id="result-bar">
                Exibindo página 
                <b>' . $this->getCurrentPage() . '</b> de 
                <b>' . $this->getTotalOfPages() . '</b> páginas para 
                <b>' . $this->getTotalOfResults() . '</b> resultados. </div>';
        }   
    } 

	public function printNavigationBar()  
    { 
        $current_page = $this->getCurrentPage();
        $total_of_pages = $this->getTotalOfPages();
        $paginator = $this->getPaginator();
        $query_string = $this->rebuildQueryString( $paginator );
        $range = $this->getRange();
        
        if($this->getTotalOfResults() > 0) {         
            echo '<ul class="pagination">'; 

            if ( $current_page > 1 ) { 
                //FIRST
                echo '<li><a onclick="pagination(1);"><span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span></a></li>';
                //PREVIOUS
                $previous = $current_page - 1; 
                echo '<li><a onclick="pagination(' . $previous . ');" aria-label="Previous"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span></a></li>';
            } 
             
            for ( 
                $x = ( $current_page - $range ); 
                $x < ( ( $current_page + $range ) + 1 ); 
                $x++ 
            ) { 
                if ( ( $x > 0 ) && ( $x <= $total_of_pages ) ) { 
                    if ( $x == $current_page ) { 
                        echo "<li class='active'><a>" . $x . "</a></li>";
                    } else { 
                        echo '<li><a onclick="pagination(' . $x . ');">' . $x . '</a></li>';
                    } 
                } 
            } 
             
            if ( $current_page != $total_of_pages ) { 
                $next = $current_page + 1; 

                //NEXT
                echo '<li><a onclick="pagination(' . $next . ');"><span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a></li>';
                //LAST
                $previous = $current_page - 1; 
                echo '<li><a onclick="pagination(' . $total_of_pages . ');" aria-label="Last"><span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span></a></li>';
            } 
             
            echo '</ul>';             
        }     
    }  
}